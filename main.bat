IF NOT EXIST venv\ GOTO install_venv 
:install_reqirements
venv\Scripts\pip.exe install -r requirements.txt
venv\Scripts\python.exe main.py
pause
exit

:install_venv
pip install virtualenv
virtualenv venv
GOTO install_reqirements